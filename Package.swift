// swift-tools-version:5.3

import PackageDescription

let package = Package(
    name: "MXAutoLayout",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "MXAutoLayout",
            targets: ["MXAutoLayout"]),
    ],
    targets: [
        .binaryTarget(
            name: "MXAutoLayout",
            path: "./Sources/MXAutoLayout.xcframework"
        )
    ]
)
